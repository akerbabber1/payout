import sys
sys.path.append('../src')
import nice_number
import unittest

class TestIsNiceNum(unittest.TestCase):
    def test_zero(self):

        actual = nice_number.is_nice_num(0)
        expected = False
        self.assertEqual(expected, actual)

    def test_negative(self):

        actual = nice_number.is_nice_num(-10)
        expected = False
        self.assertEqual(expected, actual)

    def test_true_nice_num_below1000(self):

        actual = nice_number.is_nice_num(100)
        expected = True
        self.assertEqual(expected, actual)

    def test_true_nice_num_above1000(self):

        actual = nice_number.is_nice_num(1500)
        expected = True
        self.assertEqual(expected, actual)
    
    def test_float_num(self):

        actual = nice_number.is_nice_num(1.3)
        expected = False
        self.assertEqual(expected, actual)


class TestGetNiceNum(unittest.TestCase):

    def test_zero(self):

        actual = nice_number.get_nice_num(0)
        expected = []
        self.assertEqual(expected, actual)

    def test_one(self):

        actual = nice_number.get_nice_num(1)
        expected = [1]
        self.assertEqual(expected, actual)        

    def test_negative(self):

        actual = nice_number.get_nice_num(-10)
        expected = []
        self.assertEqual(expected, actual)

    def test_float_num(self):

        actual = nice_number.get_nice_num(1.3)
        expected = [1]
        self.assertEqual(expected, actual)

    def test_long_list(self):

        actual = nice_number.get_nice_num(20)
        expected = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20]
        self.assertEqual(expected, actual)

class TestRoundToNice(unittest.TestCase):

    def test_empty_list(self):

        actual = nice_number.round_to_nice(28, [])
        expected = []
        self.assertEqual(expected, actual)

    def test_one_elem_greater(self):

        actual = nice_number.round_to_nice(28, [1])
        expected = 1
        self.assertEqual(expected, actual)

    def test_one_elem_smaller(self):

        actual = nice_number.round_to_nice(1, [20])
        expected = []
        self.assertEqual(expected, actual)

    def test_zero_num(self):

        actual = nice_number.round_to_nice(0, [1, 2, 3, 4, 5])
        expected = []
        self.assertEqual(expected, actual)


    def test_negative_num(self):

        actual = nice_number.round_to_nice(-10, [1, 2, 3, 4, 5])
        expected = []
        self.assertEqual(expected, actual)


    def test_float_num(self):

        actual = nice_number.round_to_nice(1001.28, [1, 2, 100, 1000, 10000])
        expected = 1000
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main(exit=False)
