from prizes import get_unperfect_prize, init_prizes, spend_leftover
from buckets import init_buck_size
from utils import write_to_csv

def payout(winners, prize_pool, first_prize, entry_fee, num_buck, save_file='payout.csv'):
	unperfect_prizes = get_unperfect_prize(winners, prize_pool, first_prize, entry_fee)	
	bucket_sizes = init_buck_size(winners, num_buck)
	initial_prizes, leftover = init_prizes(unperfect_prizes, bucket_sizes)
	final_prizes, final_bucket_sizes, final_leftover = spend_leftover(initial_prizes, bucket_sizes, leftover)
	write_to_csv(final_prizes, final_bucket_sizes, final_leftover, save_file)

if __name__== "__main__":
	winners = 1250
	prize_pool = 22500
	first_prize	= 4500
	entry_fee = 5
	num_buck = 10
	payout(winners, prize_pool, first_prize, entry_fee, num_buck)